# Statistics Counter

This is a tiny module that extends Drupal's core statistics module by
adding node counts for week, month and year.

The module integrates with **Views**. If you create a View and show
fields, and select the category "Content statistics", the following
fields can be used:

- Most recent view (from core **Statistics**)
- Total views (from core **Statistics**)
- Views this month
- Views this week
- Views this year
- Views today (from core **Statistics**)

The field "Most recent view" shows the the time of the most recent
view. The other shows a count (integer).

You can also use those fields to order and filter content.

## Requirements

- **Statistics** (Drupal Core)

## Recommended modules

- **Views**

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no
configuration. When enabled, counters for week, month and year becomes
fields available to **Views**.

The three counters (week, month, year) are reset in `hook_cron`. For
this to work, [cron](https://www.drupal.org/cron) needs to run as
frequent as you want the counters reset.
