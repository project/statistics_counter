<?php

/**
 * @file
 * Statistics Counter: Views support.
 */

/**
 * Implements hook_views_data_alter().
 */
function statistics_counter_views_data_alter(&$data) {
  if (!isset($data['node_counter'])) {
    return;
  }

  // Week counts.
  $data['node_counter']['weekcount'] = [
    'title' => t('Views this week'),
    'help' => t('The total number of times the node has been viewed this week.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  // Month counts.
  $data['node_counter']['monthcount'] = [
    'title' => t('Views this month'),
    'help' => t('The total number of times the node has been viewed this month.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  // Year counts.
  $data['node_counter']['yearcount'] = [
    'title' => t('Views this year'),
    'help' => t('The total number of times the node has been viewed this year.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
}
